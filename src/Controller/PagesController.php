<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Event\EventInterface;

class PagesController extends AppController
{

    public function beforeFilter(EventInterface $event){

    }
    public function index(){
        //ここからの部分がComponentで良い感じにねじ込まれてる
        require_once('vendor/autoload.php');
        $YOUR_DOMAIN ="ykst-dev";
        $YOUR_API_KEY="a0445a729bf4434292a9dd784a6e380a6f04";
        
        $client = new \Microcms\Client(
            $YOUR_DOMAIN,  // YOUR_DOMAIN は XXXX.microcms.io の XXXX 部分
            $YOUR_API_KEY  // API Key
        );
        $members = $client->get("members");
        //ここまでの部分がComponentで良い感じにねじ込まれてる

        /*
        $members の中身
        {
            "contents": [
                {
                    "id": "lmdu-t8_pcri",
                    "createdAt": "2022-01-29T08:53:31.782Z",
                    "updatedAt": "2022-01-29T08:54:33.514Z",
                    "publishedAt": "2022-01-29T08:53:31.782Z",
                    "revisedAt": "2022-01-29T08:54:33.514Z",
                    "name": "Dさん",
                    "company": [
                        {
                            "id": "jf4e1gkqmw",
                            "createdAt": "2022-01-29T05:32:40.109Z",
                            "updatedAt": "2022-01-29T05:32:40.109Z",
                            "publishedAt": "2022-01-29T05:32:40.109Z",
                            "revisedAt": "2022-01-29T05:32:40.109Z",
                            "name": "漫画",
                            "zipcode": "1000-0001",
                            "address": "30313A",
                            "telephone": "0123-4567-0001"
                        },
                        {
                            "id": "2m82s4mad1",
                            "createdAt": "2022-01-29T05:33:38.381Z",
                            "updatedAt": "2022-01-29T05:33:38.381Z",
                            "publishedAt": "2022-01-29T05:33:38.381Z",
                            "revisedAt": "2022-01-29T05:33:38.381Z",
                            "name": "ソフ",
                            "zipcode": "1000-0002",
                            "address": "30317A"
                        }
                    ],
                    "telephone": "0123-4567-0004"
                },
                {
                    "id": "lzszsw0za0-y",
                    "createdAt": "2022-01-29T05:39:17.880Z",
                    "updatedAt": "2022-01-29T05:39:17.880Z",
                    "publishedAt": "2022-01-29T05:39:17.880Z",
                    "revisedAt": "2022-01-29T05:39:17.880Z",
                    "name": "Cさん",
                    "company": []
                },
                {
                    "id": "tezqth4-o3",
                    "createdAt": "2022-01-29T05:38:33.450Z",
                    "updatedAt": "2022-01-29T05:38:33.450Z",
                    "publishedAt": "2022-01-29T05:38:33.450Z",
                    "revisedAt": "2022-01-29T05:38:33.450Z",
                    "name": "Bさん",
                    "company": [
                        {
                            "id": "2m82s4mad1",
                            "createdAt": "2022-01-29T05:33:38.381Z",
                            "updatedAt": "2022-01-29T05:33:38.381Z",
                            "publishedAt": "2022-01-29T05:33:38.381Z",
                            "revisedAt": "2022-01-29T05:33:38.381Z",
                            "name": "ソフ",
                            "zipcode": "1000-0002",
                            "address": "30317A"
                        },
                        {
                            "id": "jf4e1gkqmw",
                            "createdAt": "2022-01-29T05:32:40.109Z",
                            "updatedAt": "2022-01-29T05:32:40.109Z",
                            "publishedAt": "2022-01-29T05:32:40.109Z",
                            "revisedAt": "2022-01-29T05:32:40.109Z",
                            "name": "漫画",
                            "zipcode": "1000-0001",
                            "address": "30313A",
                            "telephone": "0123-4567-0001"
                        }
                    ]
                },
                {
                    "id": "t8iskaeeei1",
                    "createdAt": "2022-01-29T05:38:09.998Z",
                    "updatedAt": "2022-01-29T05:38:09.998Z",
                    "publishedAt": "2022-01-29T05:38:09.998Z",
                    "revisedAt": "2022-01-29T05:38:09.998Z",
                    "name": "Aさん",
                    "company": [
                        {
                            "id": "estec3-d1w",
                            "createdAt": "2022-01-29T05:34:04.431Z",
                            "updatedAt": "2022-01-29T05:34:04.431Z",
                            "publishedAt": "2022-01-29T05:34:04.431Z",
                            "revisedAt": "2022-01-29T05:34:04.431Z",
                            "name": "文化",
                            "address": "30314A"
                        }
                    ],
                    "telephone": "0123-4567-1001"
                }
            ],
            "totalCount": 4,
            "offset": 0,
            "limit": 10
        }
        */
        

        
        /*
        聞きたいこと
        ・これらの行為をViewで毎回行うのは見栄えが良いのか?
            ・property_exists (CMSで値を入れなかったら、プロパティごとなかったことにされる (Nullなり''が入っててほしい気がする))
            ・日付の表示フォーマット        
            ・"company" の中に特定の"address"が入っているかの確認
            ・"company" の中身が被参照されている "members" を検索して日付順に並べ替える


        ・Viewで表示する内容に準拠したオブジェクトをControllerで作成して、それをViewに渡したい
            ->Viewで毎回property_existsをする必要がなくなる
            ->Viewでデータを加工する必要がなくなる
            ->CMSを作り直そう(MicroCMS->手製)みたいな話もでており...
            ->Classを作れば解決しそうだけど、class PagesController extends AppControllerの中でClassは作れなさそう?でどうしよう。    
        
        */

        /* 
        class member
        {
            "id": "",
            "name": "",
            "company": [],
            "telephone": ""
        }
        class company
        {
            "id": "",
            "name": "",
            "zipcode": "",
            "address": ""
        }
        */


        /*
        $members の中身をこうしたい!!!!!!
        {
            "contents": [
                {
                    "id": "lmdu-t8_pcri",
                    "createdAt": "2022-01-29T08:53:31.782Z",
                    "updatedAt": "2022-01-29T08:54:33.514Z",
                    "publishedAt": "2022-01-29T08:53:31.782Z",
                    "revisedAt": "2022-01-29T08:54:33.514Z",
                    "name": "Dさん",
                    "company": [
                        {
                            "id": "jf4e1gkqmw",
                            "createdAt": "2022-01-29T05:32:40.109Z",
                            "updatedAt": "2022-01-29T05:32:40.109Z",
                            "publishedAt": "2022-01-29T05:32:40.109Z",
                            "revisedAt": "2022-01-29T05:32:40.109Z",
                            "name": "漫画",
                            "zipcode": "1000-0001",
                            "address": "30313A",
                            "telephone": "0123-4567-0001"
                        },
                        {
                            "id": "2m82s4mad1",
                            "createdAt": "2022-01-29T05:33:38.381Z",
                            "updatedAt": "2022-01-29T05:33:38.381Z",
                            "publishedAt": "2022-01-29T05:33:38.381Z",
                            "revisedAt": "2022-01-29T05:33:38.381Z",
                            "name": "ソフ",
                            "zipcode": "1000-0002",
                            "address": "30317A",
        --->                "telephone": null
                        }
                    ],
                    "telephone": "0123-4567-0004"
                },
                {
                    "id": "lzszsw0za0-y",
                    "createdAt": "2022-01-29T05:39:17.880Z",
                    "updatedAt": "2022-01-29T05:39:17.880Z",
                    "publishedAt": "2022-01-29T05:39:17.880Z",
                    "revisedAt": "2022-01-29T05:39:17.880Z",
                    "name": "Cさん",
                    "company": [],
        --->        "telephone": null
                },
                {
                    "id": "tezqth4-o3",
                    "createdAt": "2022-01-29T05:38:33.450Z",
                    "updatedAt": "2022-01-29T05:38:33.450Z",
                    "publishedAt": "2022-01-29T05:38:33.450Z",
                    "revisedAt": "2022-01-29T05:38:33.450Z",
                    "name": "Bさん",
                    "company": [
                        {
                            "id": "2m82s4mad1",
                            "createdAt": "2022-01-29T05:33:38.381Z",
                            "updatedAt": "2022-01-29T05:33:38.381Z",
                            "publishedAt": "2022-01-29T05:33:38.381Z",
                            "revisedAt": "2022-01-29T05:33:38.381Z",
                            "name": "ソフ",
                            "zipcode": "1000-0002",
                            "address": "30317A"
        --->                "telephone": null
                        },
                        {
                            "id": "jf4e1gkqmw",
                            "createdAt": "2022-01-29T05:32:40.109Z",
                            "updatedAt": "2022-01-29T05:32:40.109Z",
                            "publishedAt": "2022-01-29T05:32:40.109Z",
                            "revisedAt": "2022-01-29T05:32:40.109Z",
                            "name": "漫画",
                            "zipcode": "1000-0001",
                            "address": "30313A",
                            "telephone": "0123-4567-0001"
                        }
                    ],
        --->        "telephone": null
                },
                {
                    "id": "t8iskaeeei1",
                    "createdAt": "2022-01-29T05:38:09.998Z",
                    "updatedAt": "2022-01-29T05:38:09.998Z",
                    "publishedAt": "2022-01-29T05:38:09.998Z",
                    "revisedAt": "2022-01-29T05:38:09.998Z",
                    "name": "Aさん",
                    "company": [
                        {
                            "id": "estec3-d1w",
                            "createdAt": "2022-01-29T05:34:04.431Z",
                            "updatedAt": "2022-01-29T05:34:04.431Z",
                            "publishedAt": "2022-01-29T05:34:04.431Z",
                            "revisedAt": "2022-01-29T05:34:04.431Z",
                            "name": "文化",
        --->                "zipcode": null,
                            "address": "30314A",
        --->                "telephone": null
                        }
                    ],
                    "telephone": "0123-4567-1001"
                }
            ],
            "totalCount": 4,
            "offset": 0,
            "limit": 10
        }
        */





        $this->set(compact('members'));
        $this->viewBuilder()->setLayout('layout_sample');
        return $this->render();
    }

}
